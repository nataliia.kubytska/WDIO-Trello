pipeline {
  agent any

  environment {
    //content for dotenv files
    DOTENV_UI_EMAIL = credentials('email-dotenv-creds')
    DOTENV_UI_PASSWORD = credentials('password-doenv-creds')
    DOTENV_API_TOKEN = credentials('token-dotenv-api-cred')
  }

  stages {
    // runs linters before test
    stage('Lint') {
      steps {
        script {
          bat 'npm install'
          bat 'npm run lint'
        }
      }
    }
    // creates .env file with global credentials as a content and runs UI tests 
    stage('Run-tests') {
      parallel {
        stage('UI-test') {
          steps {
            // ignores if some ui tests failed (which will, because of the CAPTCHA)
            catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
              script {
                def fileContent = DOTENV_UI_EMAIL + '\n' + DOTENV_UI_PASSWORD
                writeFile(file: 'WDIO-Trello/.env', text:  fileContent)
                bat '''
                  cd WDIO-Trello
                  npm install
                '''
                bat '''
                  cd WDIO-Trello
                  npm test
                '''
              }
            }
          }
        }
        // creates .env file with global credentials as a content and runs API tests 
        stage('API-test') {
          steps {
            script {
              writeFile(file: 'API-Dropbox/.env', text:  DOTENV_API_TOKEN)
              bat '''
                cd API-Dropbox
                npm install
              '''
              bat '''
                cd API-Dropbox
                npm test
              '''
            }
          }
        }
      }
    }
    // runs BDD tests and archives the test report artifact
    stage('BDD-test') {
      steps {
        // ignores if some ui tests failed (which will, because of the CAPTCHA)
        catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
          script {
            def fileContent = DOTENV_UI_EMAIL + '\n' + DOTENV_UI_PASSWORD
            writeFile(file: 'BDD-Cucumber/.env', text:  fileContent)
            bat '''
              cd BDD-Cucumber
              npm install
            '''
            bat '''
              cd BDD-Cucumber
              npm test
            '''
          }
        }
      }
    }
  }

  post {
    failure {
      // If any of the parallel stages fail, the build will be marked as failed
      error('One or more parallel stages failed')
    }
    always {
      // retireves master test report as anrtifact of a pipeline
      archiveArtifacts artifacts: "BDD-Cucumber/src/support/reports/html-reports/master-report.html", fingerprint: true
      archiveArtifacts artifacts: "BDD-Cucumber/src/support/reports/html-reports/report-styles.css", fingerprint: true
      archiveArtifacts artifacts: "WDIO-Trello/src/support/reports/html-reports/master-report.html", fingerprint: true
      archiveArtifacts artifacts: "WDIO-Trello/src/support/reports/html-reports/report-styles.css", fingerprint: true
    }
  }
}
