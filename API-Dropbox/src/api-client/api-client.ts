import { BASE_URL, CONTENT_BASE_URL, INVALID_TOKEN } from "./api-data";
import { PostContentArguments } from "../resources/request-arguments";
import axios, { AxiosInstance } from "axios";
import * as dotenv from "dotenv";
dotenv.config();

const dropboxInstance = (token: string): AxiosInstance =>
  axios.create({
    baseURL: BASE_URL,
    timeout: 3000,
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

const contentInstance = (args: PostContentArguments): AxiosInstance =>
  axios.create({
    baseURL: CONTENT_BASE_URL,
    timeout: 3000,
    headers: {
      Authorization: `Bearer ${process.env.TOKEN}`,
      ...args,
    },
  });

export const postDropbox = async (route: string, args: object) => {
  return await dropboxInstance(process.env.TOKEN).post(route, args);
};

export const postUnathorized = async (route: string, args: object) => {
  return await dropboxInstance(INVALID_TOKEN).post(route, args);
};

export const postContent = async (args: PostContentArguments, route: string, body: Buffer) => {
  return await contentInstance(args).post(route, body);
};
