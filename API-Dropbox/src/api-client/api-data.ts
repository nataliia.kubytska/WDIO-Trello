export const BASE_URL = "https://api.dropboxapi.com/2/files";
export const CONTENT_BASE_URL = "https://content.dropboxapi.com/2/files";

export const INVALID_TOKEN = "invalid";
