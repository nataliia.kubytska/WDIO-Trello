import { postDropbox, postContent, postUnathorized } from "./api-client";
import {
  fileUploadArguments,
  Arguments,
  AddTagArguments,
  GetFolderArguments,
  GetTagArguments,
  GetMetadataArguments,
} from "../resources/request-arguments";
import { Path } from "../resources/constants";

export const uploadFile = async (route: string, body: Buffer) => {
  return await postContent(fileUploadArguments(Path["NewFile"]), route, body);
};

export const invalidUploadFile = async (route: string, body: Buffer) => {
  return await postContent(fileUploadArguments(Path["Folder"]), route, body);
};

export const uploadFileOnTopOfExisting = async (route: string, body: Buffer) => {
  return await postContent(fileUploadArguments(Path["ExistingFile"]), route, body);
};

export const addTag = async (route: string, args: AddTagArguments) => {
  return await postDropbox(route, args);
};

export const getTag = async (route: string, args: GetTagArguments) => {
  return await postDropbox(route, args);
};

export const getFileMetadata = async (route: string, args: GetMetadataArguments) => {
  return await postDropbox(route, args);
};

export const getFileMetadataUnauthorized = async (route: string, args: GetMetadataArguments) => {
  return await postUnathorized(route, args);
};

export const getFolder = async (route: string, args: GetFolderArguments) => {
  return await postDropbox(route, args);
};

export const deleteFile = async (route: string, args: Arguments) => {
  return await postDropbox(route, args);
};
