import { getFileMetadata, getFileMetadataUnauthorized, invalidUploadFile, uploadFileOnTopOfExisting } from "../api-client/business-functions";
import { metadataGettingArguments } from "../resources/request-arguments";
import { HttpStatusCode, Route, Path } from "../resources/constants";
import * as fs from "fs";
import { expect } from "chai";

describe("Negative requests", async () => {
  let fileBuffer: Buffer;

  before(async () => {
    fileBuffer = await fs.promises.readFile("src/resources/smiling-cat-3.jpg");
  });

  it("should create a conflict with a server when executing file uploading request without specified path argument", async () => {
    try {
      await invalidUploadFile(Route["FileUpload"], fileBuffer);
    } catch (error) {
      expect(error.response.status).to.equal(HttpStatusCode["Conflict"]);
    }
  });

  it("should create a conflict with a server when trying to load a new file into the path of an existing one", async () => {
    try {
      await uploadFileOnTopOfExisting(Route["FileUpload"], fileBuffer);
    } catch (error) {
      expect(error.response.status).to.equal(HttpStatusCode["Conflict"]);
    }
  });

  it("should create a bad request when trying get file metadata using arguments with invalid syntax", async () => {
    try {
      await getFileMetadata(Route["GetMetadata"], metadataGettingArguments(Path["Invalid"]));
    } catch (error) {
      expect(error.response.status).to.equal(HttpStatusCode["BadRequest"]);
    }
  });

  it("should fail request when trying get file metadata unauthorized", async () => {
    try {
      await getFileMetadataUnauthorized(Route["GetMetadata"], metadataGettingArguments(Path["ExistingFile"]));
    } catch (error) {
      expect(error.response.status).to.equal(HttpStatusCode["Unauthorized"]);
    }
  });
});
