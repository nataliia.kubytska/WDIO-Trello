import { uploadFile, addTag, getTag, getFileMetadata, getFolder, deleteFile } from "../api-client/business-functions";
import {
  TAG_ADDING_ARGUMENTS,
  TAG_GETTING_ARGUMENTS,
  FOLDER_GETTING_ARGUMENTS,
  FILE_DELETING_ARGUMENTS,
  metadataGettingArguments,
} from "../resources/request-arguments";
import { NEW_FILE_NAME, TAG_NAME, HttpStatusCode, Route, Path } from "../resources/constants";
import * as fs from "fs";
import { expect } from "chai";

describe("Endpoint operations", async () => {
  let FILE_ID: string;
  let ADDED_FILE_METADATA: object;

  it("should upload a new file provided in the request", async () => {
    const fileBuffer = await fs.promises.readFile("src/resources/smiling-cat-3.jpg");
    const response = await uploadFile(Route["FileUpload"], fileBuffer);

    FILE_ID = response.data.id;

    expect(response.status).to.equal(HttpStatusCode["Success"]);
    expect(response.data.name).to.equal(NEW_FILE_NAME);
  }).timeout(5000);

  it("must add a tag to a new file in one request and access it in another request", async () => {
    await addTag(Route["AddTag"], TAG_ADDING_ARGUMENTS);
    const response = await getTag(Route["GetTag"], TAG_GETTING_ARGUMENTS);

    expect(response.data.paths_to_tags[0].tags[0].tag_text).to.equal(TAG_NAME);
    expect(response.status).equal(HttpStatusCode["Success"]);
  });

  it("should return methadata for a new file", async () => {
    const response = await getFileMetadata(Route["GetMetadata"], metadataGettingArguments(Path["NewFile"]));

    ADDED_FILE_METADATA = response.data;

    expect(response.headers["content-type"]).to.equal("application/json");
    expect(response.status).to.equal(HttpStatusCode["Success"]);
    expect(response.data.name).to.equal(NEW_FILE_NAME);
    expect(response.data.id).to.equal(FILE_ID);
  });

  it("should verify a folder contents updating with a new file", async () => {
    const response = await getFolder(Route["GetFolder"], FOLDER_GETTING_ARGUMENTS);

    expect(response.headers["content-type"]).to.equal("application/json");
    expect(response.status).equal(HttpStatusCode["Success"]);
    expect(response.data.entries).to.deep.include(ADDED_FILE_METADATA);
  });

  it("should delete a new file", async () => {
    const response = await deleteFile(Route["DeleteFile"], FILE_DELETING_ARGUMENTS);

    expect(response.status).equal(HttpStatusCode["Success"]);
  });

  it("should verify that a new file was deleted from a folder", async () => {
    const response = await getFolder(Route["GetFolder"], FOLDER_GETTING_ARGUMENTS);

    expect(response.headers["content-type"]).to.equal("application/json");
    expect(response.status).equal(HttpStatusCode["Success"]);
    expect(response.data.entries).to.not.deep.include(ADDED_FILE_METADATA);
  });
});
