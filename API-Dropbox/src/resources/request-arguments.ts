import { Path, TAG_NAME } from "./constants";

export interface PostContentArguments {
  "Dropbox-API-Arg": string;
  "Content-Type": string;
}

export interface GetTagArguments {
  paths: string[];
}

export interface Arguments {
  path: string;
}

export interface AddTagArguments extends Arguments {
  tag_text: string;
}

export interface GetFolderArguments extends Arguments {
  include_deleted: boolean;
  include_has_explicit_shared_members: boolean;
  include_media_info: boolean;
  include_mounted_folders: boolean;
  include_non_downloadable_files: boolean;
  recursive: boolean;
}

export interface GetMetadataArguments extends Arguments {
  include_deleted: boolean;
  include_has_explicit_shared_members: boolean;
  include_media_info: boolean;
}

export const TAG_ADDING_ARGUMENTS: AddTagArguments = {
  path: Path["NewFile"],
  tag_text: TAG_NAME,
};

export const TAG_GETTING_ARGUMENTS: GetTagArguments = {
  paths: [Path["NewFile"]],
};

export const FILE_DELETING_ARGUMENTS: Arguments = {
  path: Path["NewFile"],
};

export const FOLDER_GETTING_ARGUMENTS: GetFolderArguments = {
  include_deleted: false,
  include_has_explicit_shared_members: false,
  include_media_info: false,
  include_mounted_folders: true,
  include_non_downloadable_files: true,
  path: Path["Folder"],
  recursive: false,
};

export const fileUploadArguments = (path: string): PostContentArguments => {
  return {
    "Dropbox-API-Arg": JSON.stringify({
      path: path,
      mode: "add",
      autorename: false,
      mute: false,
      strict_conflict: false,
    }),
    "Content-Type": "application/octet-stream",
  };
};

export const metadataGettingArguments = (path: string): GetMetadataArguments => {
  return {
    include_deleted: false,
    include_has_explicit_shared_members: false,
    include_media_info: false,
    path: path,
  };
};
