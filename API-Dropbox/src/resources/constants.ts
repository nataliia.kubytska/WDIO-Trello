export const TAG_NAME = "my_smiling_cat_3";
export const NEW_FILE_NAME = "smiling-cat-3.jpg";

export enum HttpStatusCode {
  Success = 200,
  BadRequest = 400,
  Unauthorized = 401,
  Conflict = 409,
}

export enum Route {
  FileUpload = "/upload",
  AddTag = "/tags/add",
  GetTag = "/tags/get",
  GetMetadata = "/get_metadata",
  GetFolder = "/list_folder",
  DeleteFile = "/delete_v2",
}

export enum Path {
  Folder = "/smiling cats",
  NewFile = "/smiling cats/smiling-cat-3.jpg",
  ExistingFile = "/smiling cats/smiling-cat-1.jpg",
  Invalid = "some path",
}
