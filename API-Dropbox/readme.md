<h2>API testing of Dropbox appication</h1>.

<h4>Link to the Dropbox API documentation: https://www.dropbox.com/developers/documentation/http/documentation</h3>

---

<h4>Tests created and implemented using Mocha framework, Chai assertion library and Axios promise-based HTTP library.</h3>

---

<h3>The task for creating test scenarios is as follows:</h3>

---

1. Implement at least 3 test scenarios using one of JS libraries like Axios, Got, SuperTest (it will be a plus) or Postman :
   · Upload file
   · Get File Metadata
   · Delete file

2. Add assertions for headers, status code and body of response
3. Add script into package.json to run API tests through the CLI (if tests done in Postman use Newman tool)

<h3>Run API tests</h3>

---

<h5>For running Api test you need to sign up/sign in to the Dropbox account and create the app. To this follow these steps:</h5>

1. Create .env file in the core of a project and insert code:

```javascript
TOKEN =
  //here you insert the token that you will learn how to generate in the next steps;
```

2. Go to https://www.dropbox.com/login.
3. Sign in to a Dropbox account using the following credentials:

- email: `test.user010101111@gmail.com`
- password: `test.password1`

4. Go to https://www.dropbox.com/developers/apps/info/rtoazsrtmjyn20o
5. Find the button `Generate` in Generated access token section on the opened Settings page.
6. Generate a new temporary token, copy it and paste it in place `//here you insert the token that you will learn how to generate in the next steps` in the `.env` file. You should have something like this (note that the token provided in the example will not work):

```javascript
TOKEN =
  sl.BxUZiPpTdjs6iOZ5IdYlKkbUMRxqXnbN_bsUDlrJRBZI2iWC8GCT5ETodbNE8jWOdjJPbBlOuy24Rh9U7mvpYaMFiLj5UuhrXSLgsMmszFdRX8IA4PDFA6fGvHifsCsGgcWS_tokGreUq8ey8orA;
```

<h5>Now to run tests simply navigate to the project folder in the command terminal and run:</h5>

```bash
npm test
```
