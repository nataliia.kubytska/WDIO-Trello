# Test Automation project

## Repository includes three projects:

### 1. Trello UI testing using WebdriverIO framework

    - Stack: WDIO/mocha/chai/spec/html-nice-reporter/wdio-reportportal-reporter

    - Directory folder: WDIO-Trello/

### 2. Trello UI testing using a BDD approach using combination of WebdriverIO and Cucumber frameworks

    - Stack: WDIO/Cucumber/chai/spec/html-nice-reporter

    - Directory folder: BDD-Cucumber/

### 3. Dropbox API testing

    - Stack: axios/mocha/chai

    - Directory folder: API-Dropbox/

## To learn more about any project, find the readme documentation in the project's root directory.
