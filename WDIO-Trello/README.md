<h2>UI testing of Trello application functionality</h2>

<h3>For UI testing WebdriverIO automation framework is implemented. Tests created using Mocha framework and Chai assertion library.</h3>

---

<h3>Run UI wdio tests</h3>

---

1. Add .env file to the core of the project and insert the following content:

```javascript
EMAIL = test.user010101111@gmail.com
PASSWORD = test.password

REPORT_PORTAL_USERNAME = default
REPORT_PORTAL_PASSWORD = Jeffwaitsinthefire_
TOKEN = trello-test-report_GfeqPJpNRzir5O7KXjABAKTcAkHMppAE0Z8vskXBv1Zv4Pjc1CXYz_YpyJaFHIHs
```

2. Run the tests using one of the following commands:

- Execute this command for all tests run:

```bash
npm test
```

- Execute this command for test that run in parallel:

```bash
npm run test-parallel
```

- There are two suites that need to be run sequentially so as not to break them in the process. To run only those execute command:

```bash
npm run test-sequential
```

<h3>See the reports</h3>

---

For report creating two reporters are implemented:

1. `wdio-html-nice-reporter` - after test execuiton ends, it creates reports folder with master-report.html file, that can be opened in a browser.

- To find the file report for parallel tests execution navigate to the `src/support/reports/html-reports/parallel`

- To find the file report for sequential tests execution navigate to the `src/support/reports/html-reports/sequential`

2. `wdio-reportportal-reporter` - after test execution ends, it prints link to the Console,which leads to the test execution report that can be opened in the browser.
   Link is printed right before the spec report, that always begins with: `"spec" Reporter:`.

Link to the reporter prints like this:

```bash
Report portal link http://localhost:8080/ui/#default_personal/launches/all/{launch count}
```

<h4>To be able to enable Report Portal on your local machine proceed with the following steps:</h4>

---

- If you don't have Docker installed:

1. [Download](https://docs.docker.com/get-docker/) and install Docker. It’s supported by all major Linux distributions, MacOS and Windows.

2. Download the latest ReportPortal Docker compose file from [GitHub](https://github.com/reportportal/reportportal/blob/master/docker-compose.yml). Or you can make it by running the following command:
   `curl -LO https://raw.githubusercontent.com/reportportal/reportportal/master/docker-compose.yml`

Ensure you override the UAT Service environment variable: `RP_INITIAL_ADMIN_PASSWORD`

3. Navigate to folder with the docker-composer.yml file on your command line and start the application using the following command: docker-compose -p reportportal up -d --force-recreate

4. Run tests and open report portal with execution results through the link in the console.

5. Login to the account using credentials from .env file:

```javascript
REPORT_PORTAL_USERNAME = default
REPORT_PORTAL_PASSWORD = Jeffwaitsinthefire_
```

- If you have Docker installed on your local machine: skip the first step and proceed with the rest of the instructions above.

- If you don't want to install Docker on your local machine: ignore Report Portal error messages at the start of the test run and view the reports with the WDIO html-nice reporter alone.
