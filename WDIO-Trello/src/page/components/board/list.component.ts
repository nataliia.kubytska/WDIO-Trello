import BaseComponent from "../common/base.component.ts";
import Button from "../../controls/button.ts";

class ListComponent extends BaseComponent {
  constructor() {
    super("[data-testid='list']");
  }

  get listName() {
    return this.rootEl.$("[data-testid='list-name']");
  }

  get cardComposerBtn() {
    return new Button("[data-testid='list-add-card-button']");
  }

  get cardsNames() {
    return this.rootEl.$$("[data-testid='card-name']");
  }
}

export default ListComponent;
