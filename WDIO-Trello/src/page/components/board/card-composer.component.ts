import BaseComponent from "../common/base.component.ts";
import Button from "../../controls/button.ts";
import Input from "../../controls/input.ts";

class CardComposerComponent extends BaseComponent {
  constructor() {
    super("form.H136XFPzM9syCb");
  }

  get cardNameInput() {
    return new Input("[data-testid='list-card-composer-textarea']");
  }

  get addCardBtn() {
    return new Button("[data-testid='list-card-composer-add-card-button']");
  }

  get cancelCardBtn() {
    return new Button("[data-testid='list-card-composer-cancel-button']");
  }
}

export default CardComposerComponent;
