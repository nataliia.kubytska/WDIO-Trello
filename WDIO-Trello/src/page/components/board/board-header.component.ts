import BaseComponent from "../common/base.component.ts";
import Button from "../../controls/button.ts";

class BoardHeaderComponent extends BaseComponent {
  constructor() {
    super(".board-header");
  }

  get boardName() {
    return this.rootEl.$("[data-testid='board-name-display']");
  }

  get filterBtn() {
    return new Button("[data-testid='filter-popover-button']");
  }

  get filterClearBtn() {
    return new Button("[data-testid='filter-popover-button-x']");
  }
}

export default BoardHeaderComponent;
