import BaseComponent from "../common/base.component.ts";
import Button from "../../controls/button.ts";

class MemberNavbarComponent extends BaseComponent {
  constructor() {
    super(".tabbed-pane-nav-member-detail-redesign");
  }

  get profileTab() {
    return new Button("[data-tab='profile']");
  }
}

export default MemberNavbarComponent;
