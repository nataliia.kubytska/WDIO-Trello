import BaseComponent from "../common/base.component.ts";
import Button from "../../controls/button.ts";
import Input from "../../controls/input.ts";

class EditUserFormComponent extends BaseComponent {
  constructor() {
    super("[data-testid='profile-form']");
  }

  get saveEditBtn() {
    return new Button("button[type='submit']");
  }

  get errorMessage() {
    return this.rootEl.$("#SaveProfileError_Field_username");
  }

  /**
   *
   * @param  name { "username" | "bio" }
   * @returns {*}
   */

  get input() {
    const selectors = {
      username: "#username",
      bio: "#bio",
    };

    return (name: "username" | "bio") => new Input(selectors[name]);
  }

  get successPopup() {
    return $("#FlagGroup .QMKgZFIlTLiEJN");
  }

  get closePopupBtn() {
    return new Button("button.LbO_k5JPG5miXd");
  }

  async closePopup() {
    await this.closePopupBtn.waitAndClick();
    await this.closePopupBtn.waitForDisappear();
  }
}

export default EditUserFormComponent;
