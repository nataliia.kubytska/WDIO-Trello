import BaseComponent from "../common/base.component.ts";
import Input from "../../controls/input.ts";

class SearchComponent extends BaseComponent {
  constructor() {
    super(".U8nNpLQtodnSyN");
  }

  get searchInput() {
    return new Input("[data-testid='advanced-search-input']");
  }

  get boards() {
    return this.rootEl.$("h3.=boards");
  }

  get searchResultItems() {
    return this.rootEl.$$("[data-testid='advanced-search-board-result-item']");
  }
}

export default SearchComponent;
