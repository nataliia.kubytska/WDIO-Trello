import BaseComponent from "../common/base.component.ts";
import Button from "../../controls/button.ts";
import Input from "../../controls/input.ts";
import { CredentialsInput, UserInputString } from "../../../support/resources/types.ts";

class SignupFormComponent extends BaseComponent {
  get signupBtn() {
    return new Button("#signup-submit");
  }

  get errorMessage() {
    return $("#email-uid2-error");
  }

  /**
   *
   * @param  name {"email" | "password"}
   * @returns {*}
   */

  get input() {
    const selectors: CredentialsInput = {
      email: "#email",
      password: "#password",
    };

    return (name: keyof typeof selectors) => new Input(selectors[name]);
  }

  async performSignup(email: UserInputString) {
    await this.input("email").waitAndSetValue(email);
    await this.signupBtn.waitAndClick();
    await this.signupBtn.waitAndClick();
  }
}

export default SignupFormComponent;
