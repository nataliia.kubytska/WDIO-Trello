import { BasePage } from "./base.page.ts";
import { LoginForm } from "../components/index.ts";

export class LoginPage extends BasePage {
  protected readonly path: string = "/login";
  readonly loginForm: LoginForm;

  constructor() {
    super();
    this.loginForm = new LoginForm();
  }
}
