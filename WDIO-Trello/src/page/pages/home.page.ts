import { BasePage } from "./base.page.ts";
import { HomeHeader } from "../components/index.ts";

export class HomePage extends BasePage {
  protected readonly path: string = "/";
  readonly homeHeader: HomeHeader;

  constructor() {
    super();
    this.homeHeader = new HomeHeader();
  }
}
