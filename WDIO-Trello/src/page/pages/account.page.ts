import { BasePage } from "./base.page.ts";
import { UserInfo, MemberNavbar, EditUserForm } from "../components/index.ts";

export class AccountPage extends BasePage {
  protected readonly path: string = "/u/testuser25489";
  readonly userInfo: UserInfo;
  readonly memberNavbar: MemberNavbar;
  readonly editUserForm: EditUserForm;

  constructor() {
    super();
    this.userInfo = new UserInfo();
    this.memberNavbar = new MemberNavbar();
    this.editUserForm = new EditUserForm();
  }
}
