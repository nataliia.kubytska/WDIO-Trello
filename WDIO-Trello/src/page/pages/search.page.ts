import { BasePage } from "./base.page.ts";
import { SearchComponent } from "../components/index.ts";

export class SearchPage extends BasePage {
  protected readonly path: string = "/search";
  readonly searchComponent: SearchComponent;

  constructor() {
    super();
    this.searchComponent = new SearchComponent();
  }
}
