import { BasePage } from "./base.page.ts";
import { AllBoards } from "../components/index.ts";

export class BoardsPage extends BasePage {
  protected readonly path: string = "/u/testuser25489/boards";
  readonly allBoards: AllBoards;

  constructor() {
    super();
    this.allBoards = new AllBoards();
  }
}
