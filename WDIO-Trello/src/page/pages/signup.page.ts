import { BasePage } from "./base.page.ts";
import { SignupForm } from "../components/index.ts";

export class SignupPage extends BasePage {
  protected readonly path: string = "/signup";
  readonly signupForm: SignupForm;

  constructor() {
    super();
    this.signupForm = new SignupForm();
  }
}
