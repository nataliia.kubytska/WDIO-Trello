import { browser, $ } from "@wdio/globals";
import { Header, CreateMenuPopover } from "../components/index.ts";

export abstract class BasePage {
  protected abstract readonly path: string;
  readonly header: Header;
  readonly createMenuPopover: CreateMenuPopover;

  protected constructor() {
    this.header = new Header();
    this.createMenuPopover = new CreateMenuPopover();
  }

  get loggedoutHeader() {
    return $("[data-testid='logged-out-header-wide']");
  }

  async open(): Promise<void> {
    // await browser.maximizeWindow();
    await browser.url(`https://trello.com${this.path}`);
  }

  async openInNewWindow(): Promise<void> {
    await browser.newWindow(`https://trello.com${this.path}`);
  }
}
