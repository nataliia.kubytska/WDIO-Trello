import { page } from "../../page/index.ts";
import { EditFormValue } from "./constants.ts";
import { WorkspaceInputName } from "./types.ts";
import { expect } from "chai";

type Page = "board" | "search" | "account" | "workspace";

const loginAndOpenPage = async (pageName: Page) => {
  await page("login").open();
  await page("login").loginForm.performLogin(process.env.EMAIL, process.env.PASSWORD);

  await page(pageName).openInNewWindow();
};

export const hooksBeforeEach = {
  login: async () => {
    await page("login").open();
    await page("login").loginForm.performLogin(process.env.EMAIL, process.env.PASSWORD);
  },

  loginAndOpenProfilePage: async () => {
    await loginAndOpenPage("account");
    await page("account").memberNavbar.profileTab.waitAndClick();

    const editUserForm = await page("account").editUserForm;
    const isRootDisplayed = await editUserForm.waitAndCheckForDisplayed(editUserForm.rootEl);
    await expect(isRootDisplayed).to.be.true;
  },

  loginAndOpenWorkspacePage: async () => {
    await loginAndOpenPage("workspace");
    await page("workspace").header.memberIcon.waitForDisplayed();
  },

  loginAndOpenSearchPage: async () => {
    await loginAndOpenPage("search");
  },

  loginAndOpenBoardPage: async () => {
    await loginAndOpenPage("board");
  },

  openSignupPage: async () => {
    await page("home").open();
    await page("home").homeHeader.signupPageBtn.waitAndClick();
  },
};

const resetProfileChanges = async (eraseValuesLength: number) => {
  const editForm = await page("account").editUserForm;
  await editForm.eraseInputValues(editForm.input("username"), eraseValuesLength);
  await editForm.saveEditBtn.waitAndClick();
};

const resetWorkspaceChanges = async (inputName: WorkspaceInputName, eraseValuesLength: number) => {
  await page("workspace").workspaceInfo.openEditWorkspaceFormBtn.waitAndClick();

  const editWorkspaceForm = await page("workspace").editWorkspaceForm;
  await editWorkspaceForm.eraseInputValues(page("workspace").editWorkspaceForm.input(inputName), eraseValuesLength);
  await editWorkspaceForm.saveEditBtn.waitAndClick();
  await page("workspace").workspaceInfo.workspaceName.waitForDisplayed();
};

export const hooksAfter = {
  afterEach: {
    reload: async () => {
      await browser.reloadSession();
    },
  },

  after: {
    resetProfileChangesValid: async () => {
      await resetProfileChanges(EditFormValue.Valid.length);

      await browser.waitUntil(async () => {
        const url = await browser.getUrl();
        return !url.includes(EditFormValue.Valid);
      });
      await browser.reloadSession();
    },

    resetProfileChangesInvalid: async () => {
      await resetProfileChanges(EditFormValue.Invalid.length);
      await browser.reloadSession();
    },

    resetWorkspaceName: async () => {
      await resetWorkspaceChanges("name", EditFormValue.Valid.length);
      await browser.reloadSession();
    },

    resetWorkspaceDescription: async () => {
      await resetWorkspaceChanges("description", EditFormValue.Valid.length);
      await browser.reloadSession();
    },
  },
};
