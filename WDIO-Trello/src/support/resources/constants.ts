export const INVALID_SIGNUP_EMAIL = "test.user010101111@gmailcom";

export const EDIT_SUCCESS_POPUP_MESSAGE = "Saved";
export const INVALID_USERNAME_MESSAGE = "Username is invalid";

export enum CardName {
  ForCreate = "Card Name",
  ForCancell = "Card Cancel",
}

export enum ListName {
  ForCreate = "List Name",
  ForCancell = "List Cancel",
}

export enum Name {
  ForCreateCard = "Card Name",
  ForCancellCard = "Card Cancel",
  ForCreateList = "List Name",
  ForCancellList = "List Cancel",
  ForCreateBoard = "Board Name",
}

export enum BoardSearchValue {
  FullName = "Board Test Search",
  PartOfName = "for test",
}

export enum EditFormValue {
  Valid = "testing",
  Invalid = "*^",
}

export enum IncorrectLoginCreds {
  Email = "test.user010101111@gmail.co",
  Password = "test.passwor",
}

enum RegExpPatterns {
  UserWorkspaceTitle = "your workspaces|ваші робочі області|ваши рабочие пространства",
  ErrorSignupMsg = "please enter a valid email address|укажіть дійсну адресу електронної пошти",
  ErrorLoginMsg = "incorrect email address and / or password|incorrecto correo|неправильный адрес электронной почты|неправильна адреса електронної пошти та (або) пароль",
  ResetPasswordSuccessMsg = "we sent a recovery link to you|ми надіслали посилання для відновлення|мы отправили ссылку для восстановления",
}

export function createRegExp(pattern: keyof typeof RegExpPatterns) {
  return new RegExp(RegExpPatterns[pattern], "i");
}
