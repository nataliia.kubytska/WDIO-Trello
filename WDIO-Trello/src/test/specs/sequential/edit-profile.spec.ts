import { page } from "../../../page/index.ts";
import { hooksBeforeEach, hooksAfter } from "../../../support/resources/hooks.ts";
import { INVALID_USERNAME_MESSAGE, EditFormValue } from "../../../support/resources/constants.ts";
import { expect } from "chai";

describe("Trello editind user profile", () => {
  beforeEach(hooksBeforeEach.loginAndOpenProfilePage);

  describe("Update username with valid characters", () => {
    it("should update username when edited using valid characters", async () => {
      const editForm = await page("account").editUserForm;
      await editForm.input("username").waitAndAddValue(EditFormValue.Valid);
      await editForm.saveEditBtn.waitAndClick();

      await browser.waitUntil(async () => {
        const url = await browser.getUrl();
        return url.includes(EditFormValue.Valid);
      });

      const popupDisplayed = await editForm.waitAndCheckForDisplayed(editForm.successPopup);
      const username = await page("account").userInfo.username.getText();

      await expect(username).to.include(EditFormValue.Valid, "username has not been updated with expected values");
      await expect(popupDisplayed, "success popup did not display").to.be.true;
    });

    after(hooksAfter.after.resetProfileChangesValid);
  });

  describe("update username with invalid characters", () => {
    it("should throw an error when username edited with invalid characters", async () => {
      const editForm = await page("account").editUserForm;
      await editForm.input("username").waitAndAddValue(EditFormValue.Invalid);
      await editForm.saveEditBtn.waitAndClick();

      const errorText = await editForm.waitAndGetText(editForm.errorMessage);
      const errorMessageDisplayed = await editForm.errorMessage.isDisplayed();

      await expect(errorMessageDisplayed, "error message did not display").to.be.true;
      await expect(errorText).to.include(INVALID_USERNAME_MESSAGE, "error message didn't include expected text");
    });

    after(hooksAfter.after.resetProfileChangesInvalid);
  });

  describe("cancel username editing", async () => {
    it("username should remain unchanged when cancel edited changes", async () => {
      const userInfo = await page("account").userInfo;
      const initialUserName = await userInfo.waitAndGetText(userInfo.username);

      await page("account").editUserForm.input("username").waitAndAddValue(EditFormValue.Valid);
      await browser.refresh();

      const finalUserName = await userInfo.waitAndGetText(userInfo.username);

      await expect(initialUserName).to.equal(finalUserName, "username changed");
      await expect(finalUserName).to.not.include(EditFormValue.Valid, "username updated even after cancelation");
    });

    afterEach(hooksAfter.afterEach.reload);
  });
});
