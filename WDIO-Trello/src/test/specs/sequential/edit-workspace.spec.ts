import { page } from "../../../page/index.ts";
import { hooksBeforeEach, hooksAfter } from "../../../support/resources/hooks.ts";
import { EditFormValue } from "../../../support/resources/constants.ts";
import { expect } from "chai";

describe("Trello editing workspace", () => {
  beforeEach(hooksBeforeEach.loginAndOpenWorkspacePage);

  describe("Update workspace name with valid characters", () => {
    it("should update workspace name when edited using valid characters", async () => {
      await page("workspace").workspaceInfo.openEditWorkspaceFormBtn.waitAndClick();

      const editWorkspaceForm = await page("workspace").editWorkspaceForm;
      await editWorkspaceForm.input("name").waitAndAddValue(EditFormValue.Valid);
      await editWorkspaceForm.saveEditBtn.waitAndClick();

      const workspaceInfo = await page("workspace").workspaceInfo;
      const workspaceName = await workspaceInfo.waitAndGetText(workspaceInfo.workspaceName);

      await expect(workspaceName).to.have.string(EditFormValue.Valid);
    });

    after(hooksAfter.after.resetWorkspaceName);
  });

  describe("Update workspace description with valid characters", () => {
    it("should update workspace name when edited using valid characters", async () => {
      await page("workspace").workspaceInfo.openEditWorkspaceFormBtn.waitAndClick();

      const editWorkspaceForm = await page("workspace").editWorkspaceForm;
      await editWorkspaceForm.input("description").waitAndAddValue(EditFormValue.Valid);
      await editWorkspaceForm.saveEditBtn.waitAndClick();

      const workspaceInfo = page("workspace").workspaceInfo;
      const workspaceDescription = await workspaceInfo.waitAndGetText(workspaceInfo.workspaceDescription);

      await expect(workspaceInfo.workspaceDescription, "description did not exist").to.exist;
      await expect(workspaceDescription).to.equal(EditFormValue.Valid);
    });

    after(hooksAfter.after.resetWorkspaceDescription);
  });

  describe("cancel workspace editing", async () => {
    it("workspace name should remain unchanged when cancel edited changes", async () => {
      const workspaceInfo = await page("workspace").workspaceInfo;
      const initialWorkspaceName = await workspaceInfo.waitAndGetText(workspaceInfo.workspaceName);

      await page("workspace").workspaceInfo.openEditWorkspaceFormBtn.waitAndClick();

      const editWorkspaceForm = await page("workspace").editWorkspaceForm;
      await editWorkspaceForm.input("description").waitAndAddValue(EditFormValue.Valid);
      await editWorkspaceForm.cancelEditBtn.waitAndClick();

      const finalWorkspaceName = await workspaceInfo.waitAndGetText(workspaceInfo.workspaceName);

      await expect(initialWorkspaceName).to.equal(finalWorkspaceName, "workspace name changed");
      await expect(finalWorkspaceName).to.not.include(EditFormValue.Valid);
    });

    afterEach(hooksAfter.afterEach.reload);
  });
});
