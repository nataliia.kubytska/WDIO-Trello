import { page } from "../../../page/index.ts";
import { hooksBeforeEach, hooksAfter } from "../../../support/resources/hooks.ts";
import { Name } from "../../../support/resources/constants.ts";
import { expect } from "chai";

describe("Trello Card Creation", () => {
  beforeEach(hooksBeforeEach.loginAndOpenBoardPage);

  it("should create a new card in list", async () => {
    await page("board").list.cardComposerBtn.waitAndClick();
    await page("board").cardComposer.cardNameInput.waitAndSetValue(Name.ForCreateCard);
    await page("board").cardComposer.addCardBtn.waitAndClick();

    const cardsNames = page("board").list.cardsNames;
    const latestCard = await page("board").list.getLastElement(await cardsNames);
    const latestCardName = await latestCard.getText();

    await expect(latestCardName).to.equal(Name.ForCreateCard, "latest card name title is not the expected one");
  });

  it("should cancel new card creation in list if requested", async () => {
    await page("board").list.cardComposerBtn.waitAndClick();
    await page("board").cardComposer.cardNameInput.waitAndSetValue(Name.ForCancellCard);
    await page("board").cardComposer.cancelCardBtn.waitAndClick();

    const allCardsNames = await page("board").list.cardsNames;
    const cardNamesArray = await allCardsNames.map(async (cardName) => {
      return await cardName.getText();
    });

    await expect(cardNamesArray).to.not.include(Name.ForCancellCard, "array contains this card name");
  });

  afterEach(hooksAfter.afterEach.reload);
});
