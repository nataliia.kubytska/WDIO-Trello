import { page } from "../../../page/index.ts";
import { IncorrectLoginCreds, createRegExp } from "../../../support/resources/constants.ts";
import { expect } from "chai";

describe("Trello Login", () => {
  beforeEach(async () => {
    await page("login").open();
  });

  it("an appropriate error message should appear when trying to log in with incorrect credentials", async () => {
    const loginForm = page("login").loginForm;
    await loginForm.performLogin(IncorrectLoginCreds.Email, IncorrectLoginCreds.Password);

    const errorMessage = await loginForm.waitAndGetText(loginForm.errorLoginMessage);

    await expect(errorMessage).to.match(createRegExp("ErrorLoginMsg"));
  });

  it("a corresponding message should appear when requesting a password reset", async () => {
    const loginForm = page("login").loginForm;

    await loginForm.input("email").waitAndSetValue(process.env.EMAIL);
    await loginForm.button("continue").waitAndClick();
    await loginForm.button("resetPasswordRequest").waitAndClick();
    await loginForm.button("resetPasswordConfirm").waitAndClick();

    const emailSentMessage = await loginForm.waitAndGetText(loginForm.emailSentMessage);
    const messageDisplayed = await loginForm.emailSentMessage.isDisplayed();

    await expect(messageDisplayed, "message did not display").to.be.true;
    await expect(emailSentMessage).to.match(createRegExp("ResetPasswordSuccessMsg"));
  });

  it("should log in with valid credentials", async () => {
    const loginForm = page("login").loginForm;
    const allBoards = page("boards").allBoards;

    await loginForm.performLogin(process.env.EMAIL, process.env.PASSWORD);

    const userWorkspacesTitle = await allBoards.waitAndGetText(allBoards.userWorkspacesTitle);

    await expect(userWorkspacesTitle).to.match(createRegExp("UserWorkspaceTitle"));
  });
});
