import { page } from "../../../page/index.ts";
import { hooksBeforeEach, hooksAfter } from "../../../support/resources/hooks.ts";
import { BoardSearchValue } from "../../../support/resources/constants.ts";
import { expect } from "chai";

describe("Trello Board Search", () => {
  beforeEach(hooksBeforeEach.loginAndOpenSearchPage);

  it("should find a board with the specified name and be able to access it", async () => {
    const search = await page("search").searchComponent;
    await search.searchInput.waitAndSetValue(BoardSearchValue.FullName);
    await search.boards.waitForDisplayed();

    const firstSearchResultItem = await search.searchResultItems[0];
    const boardTitle = await firstSearchResultItem.getAttribute("title");

    await expect(boardTitle).to.equal(BoardSearchValue.FullName);

    await firstSearchResultItem.click();
    const boardHeader = await page("board").boardHeader;
    const boardName = await boardHeader.waitAndGetText(boardHeader.boardName);

    await expect(boardName).to.equal(BoardSearchValue.FullName);
  });

  it("should find all board with similar names and be able to access it", async () => {
    const regex = new RegExp(BoardSearchValue.PartOfName, "i");

    const search = await page("search").searchComponent;
    await search.searchInput.waitAndSetValue(BoardSearchValue.PartOfName);
    await search.boards.waitForDisplayed();

    const searchResultList = await search.searchResultItems;
    const resultTitles = await searchResultList.map(async (resultItem) => {
      return await resultItem.getAttribute("title");
    });

    await expect(resultTitles.every((title) => regex.test(title))).to.be.true;
    await resultTitles.forEach((title) => expect(title).to.match(regex));

    const secondSearchResultItem = await search.searchResultItems[1];
    await secondSearchResultItem.click();

    const boardHeader = await page("board").boardHeader;
    const boardName = await boardHeader.waitAndGetText(boardHeader.boardName);

    await expect(boardName).to.match(regex);
  });

  afterEach(hooksAfter.afterEach.reload);
});
