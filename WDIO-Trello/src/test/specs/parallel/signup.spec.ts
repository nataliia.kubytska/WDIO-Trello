import { page } from "../../../page/index.ts";
import { hooksBeforeEach } from "../../../support/resources/hooks.ts";
import { INVALID_SIGNUP_EMAIL, createRegExp } from "../../../support/resources/constants.ts";
import { expect } from "chai";

describe("Trello Sign up", () => {
  beforeEach(hooksBeforeEach.openSignupPage);

  it("an error message should appear when trying to sign up with invalid credential", async () => {
    const signupForm = page("signup").signupForm;

    await signupForm.performSignup(INVALID_SIGNUP_EMAIL);
    await signupForm.signupBtn.waitAndClick();
    await signupForm.signupBtn.waitAndClick();
    await signupForm.errorMessage.waitForDisplayed();

    const messageDisplayed = await signupForm.waitAndCheckForDisplayed(signupForm.errorMessage);
    const errorMessage = await signupForm.errorMessage.getText();

    await expect(messageDisplayed, "error message did not display").to.be.true;
    await expect(errorMessage).to.match(createRegExp("ErrorSignupMsg"));
  });
});
