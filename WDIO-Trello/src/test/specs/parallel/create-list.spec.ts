import { page } from "../../../page/index.ts";
import { hooksBeforeEach, hooksAfter } from "../../../support/resources/hooks.ts";
import { Name } from "../../../support/resources/constants.ts";
import { expect } from "chai";

describe("Trello List Creation", () => {
  beforeEach(hooksBeforeEach.loginAndOpenBoardPage);

  it("should create new list in board", async () => {
    await page("board").lists.listComposerBtn.waitAndClick();
    await page("board").listComposer.listNameInput.waitAndSetValue(Name.ForCreateList);
    await page("board").listComposer.addListBtn.waitAndClick();

    const listNames = page("board").lists.listNames;
    const latestList = await page("board").lists.getLastElement(await listNames);
    const latestListName = await latestList.getText();

    await expect(latestListName).to.equal(Name.ForCreateList, "latest list name title is not the expected one");
  });

  it("should cancel new list creation in board if requested", async () => {
    await page("board").lists.listComposerBtn.waitAndClick();
    await page("board").listComposer.listNameInput.waitAndSetValue(Name.ForCancellList);
    await page("board").listComposer.cancelListBtn.waitAndClick();

    const allListNames = await page("board").lists.listNames;
    const listNamesArray = await allListNames.map(async (listName) => {
      return await listName.getText();
    });

    await expect(listNamesArray).to.not.include(Name.ForCancellList, "array contains this list name");
  });

  afterEach(hooksAfter.afterEach.reload);
});
