import { page } from "../../../page/index.ts";
import { hooksBeforeEach } from "../../../support/resources/hooks.ts";
import { Name } from "../../../support/resources/constants.ts";
import { expect } from "chai";

describe("Creating new Board", () => {
  beforeEach(hooksBeforeEach.login);

  it("should add new board with specified name", async () => {
    await page("boards").header.createMenuBtn.waitAndClick();
    await page("boards").createMenuPopover.createBoardBtn.waitAndClick();
    await page("boards").createMenuPopover.boardTitleInput.waitAndSetValue(Name.ForCreateBoard);
    await page("boards").createMenuPopover.submitBoardBtn.waitAndClick();

    const boardName = await page("board").boardHeader.boardName.getText();

    await expect(boardName).to.equal(Name.ForCreateBoard);
  });
});
