# BDD Approach for UI testing of Trello application functionality

For Trello UI testing `WebdriverIO` automation framework is implemented. Tests built using a BDD approach using the `Cucumber` framework and the `Chai` assertion library.

Tests are written as Scenarios in Gherkin syntax and serve as living documentation for main features functionality.

## Getting started

### To run WDIO Cucumber tests:

1. Add `.env` file to the core of the project and insert the following content:

```javascript
EMAIL = test.user010101111@gmail.com
PASSWORD = test.password
```

2. To add `node_modules` execute command:

```bash
npm install
```

3. Run the tests using one of the following commands:

- Execute this command to run all the scenarios:

```bash
npm test
```

- Execute this command to run only smoke scenarios:

```bash
npm run test-smoke
```

- Execute this command to run only regression scenarios:

```bash
npm run test-regression
```

- Execute this command to run only negative scenarios:

```bash
npm run test-negative
```

- Execute this command to run scenarios in parallel:

```bash
npm run test-parallel
```

- There are two features that need to be run sequentially so as not to break them in the process. To run only those execute command:

```bash
npm run test-sequential
```

## See the reports

For result reporting `wdio-html-nice-reporter` is implemented.

It creates reports folder with `master-report.html` file, that can be opened in a browser.

To find the file report for features execution results navigate to the `src/support/reports/html-reports`.
