export const EDIT_FORM_INPUT_VALUE = "testing";
export const TRELLO_USERNAME = "testuser25489";
export const WORKSPACE_NAME = "Test Workspace";

enum RegExpPatterns {
  USER_WORKSPACE_TITLE = "your workspaces|ваші робочі області|ваши рабочие пространства",
  ERROR_SIGNUP_MESSAGE = "please enter a valid email address|укажіть дійсну адресу електронної пошти",
  ERROR_LOGIN_MESSAGE = "incorrect email address and / or password|incorrecto correo|неправильный адрес электронной почты|неправильна адреса електронної пошти та (або) пароль",
  RESET_PASSWORD_SUCCESS_MESSAGE = "we sent a recovery link to you|ми надіслали посилання для відновлення|мы отправили ссылку для восстановления",
}

export function createRegExp(pattern: keyof typeof RegExpPatterns) {
  return new RegExp(RegExpPatterns[pattern], "i");
}
