import { page } from "../../page/index.ts";
import { EDIT_FORM_INPUT_VALUE, TRELLO_USERNAME, WORKSPACE_NAME } from "./constants.ts";
import { WorkspaceInputName, UserInputString } from "./types.ts";

export const reload = async function () {
  await browser.reloadSession();
};

// AFter hook for profile username editing reset
export const resetProfileUsername = async function () {
  await page("account").editUserForm.input("username").waitAndSetValue(TRELLO_USERNAME);
  await page("account").editUserForm.saveEditBtn.waitAndClick();

  await browser.waitUntil(async () => {
    const url = await browser.getUrl();
    return !url.includes(EDIT_FORM_INPUT_VALUE);
  });
  await browser.reloadSession();
};

// AFter hooks for workspace editing reset
const resetWorkspaceChanges = async function (input: WorkspaceInputName, value: UserInputString) {
  await page("workspace").workspaceInfo.openEditWorkspaceFormBtn.waitAndClick();
  await page("workspace").editWorkspaceForm.input(input).waitAndSetValue(value);
  await page("workspace").editWorkspaceForm.saveEditBtn.waitAndClick();
  await page("workspace").workspaceInfo.workspaceName.waitForDisplayed();
};

export const resetWorkspaceName = async function () {
  await resetWorkspaceChanges("name", WORKSPACE_NAME);
  await browser.reloadSession();
};

export const resetWorkspaceDescription = async function () {
  const erase = "\u00A0";

  await resetWorkspaceChanges("description", erase);
  await browser.reloadSession();
};
