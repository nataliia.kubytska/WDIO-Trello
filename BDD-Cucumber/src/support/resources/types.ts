export type WorkspaceInputName = "name" | "description";
export type UserInputString = string;
export type ElementSelectorString = string;

export interface CredentialsInput {
  email: ElementSelectorString;
  password: ElementSelectorString;
}
