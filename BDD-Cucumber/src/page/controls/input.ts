import { $ } from "@wdio/globals";

class Input {
  protected readonly selector: string;

  constructor(selector: string) {
    this.selector = selector;
  }

  get element() {
    return $(this.selector);
  }

  async waitAndClick() {
    await this.element.waitForExist({
      timeout: 10000,
      timeoutMsg: "Input did not exist",
    });
    await this.element.waitForDisplayed({
      timeout: 10000,
      timeoutMsg: "Input did not display",
    });
    await this.element.waitForEnabled({
      timeout: 10000,
      timeoutMsg: "Input is not enabled",
    });
    await this.element.click();
  }

  async waitAndSetValue(value: string) {
    await this.element.waitForExist({
      timeout: 10000,
      timeoutMsg: "Input did not exist",
    });
    await this.element.waitForDisplayed({
      timeout: 10000,
      timeoutMsg: "Input did not display",
    });
    await this.element.waitForEnabled({
      timeout: 10000,
      timeoutMsg: "Input is not enabled",
    });
    await this.element.setValue(value);
  }

  async waitAndAddValue(value: string) {
    await this.element.waitForExist({
      timeout: 10000,
      timeoutMsg: "Input did not exist",
    });
    await this.element.waitForDisplayed({
      timeout: 10000,
      timeoutMsg: "Input did not display",
    });
    await this.element.waitForEnabled({
      timeout: 10000,
      timeoutMsg: "Input is not enabled",
    });
    await this.element.addValue(value);
  }
}

export default Input;
