import { $ } from "@wdio/globals";
import BaseComponent from "../common/base.component.ts";
import Button from "../../controls/button.ts";
import Input from "../../controls/input.ts";
import { UserInputString, CredentialsInput, ElementSelectorString } from "../../../support/resources/types.ts";

interface LoginCredentialsInput extends CredentialsInput {
  emailForPasswordReset: ElementSelectorString;
}
type ButtonName = "continue" | "login" | "resetPasswordRequest" | "resetPasswordConfirm";

class LoginFormComponent extends BaseComponent {
  get errorLoginMessage() {
    return $("[data-testid='form-error']");
  }

  get emailSentMessage() {
    return $("#email-sent-page");
  }

  /**
   *
   * @param  name {"continue" | "login" | "resetPasswordRequest" | "resetPasswordConfirm"}
   * @returns {*}
   */

  get button() {
    const selectors = {
      continue: "#login",
      login: "#login-submit",
      resetPasswordRequest: "#resetPassword",
      resetPasswordConfirm: "#reset-password-email-submit",
    };

    return (name: ButtonName) => new Button(selectors[name]);
  }

  /**
   *
   * @param  name {"email" | "password" | "emailForPasswordReset"}
   * @returns {*}
   */

  get input() {
    const selectors: LoginCredentialsInput = {
      email: "#user",
      password: "#password",
      emailForPasswordReset: "#email",
    };

    return (name: keyof typeof selectors) => new Input(selectors[name]);
  }

  async performLogin(email: UserInputString, password: UserInputString): Promise<void> {
    await this.input("email").waitAndSetValue(email);
    await this.button("continue").waitAndClick();
    await this.button("continue").waitForDisappear();
    await this.input("password").waitAndSetValue(password);
    await this.button("login").waitAndClick();
  }

  async passwordResetRequest(email: UserInputString): Promise<void> {
    await this.input("email").waitAndSetValue(email);
    await this.button("continue").waitAndClick();
    await this.button("resetPasswordRequest").waitAndClick();
    await this.button("resetPasswordConfirm").waitAndClick();
  }
}

export default LoginFormComponent;
