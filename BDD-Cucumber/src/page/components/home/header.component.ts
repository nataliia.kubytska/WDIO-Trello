import BaseComponent from "../common/base.component.ts";
import Button from "../../controls/button.ts";

class HomeHeaderComponent extends BaseComponent {
  constructor() {
    super("header[data-testid='bignav']");
  }

  get signupPageBtn() {
    return new Button("[data-uuid='MJFtCCgVhXrVl7v9HA7EH_signup']");
  }

  get loginPageBtn() {
    return new Button("[data-uuid='MJFtCCgVhXrVl7v9HA7EH_login']");
  }
}

export default HomeHeaderComponent;
