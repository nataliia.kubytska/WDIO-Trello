import BaseComponent from "./base.component.ts";
import Button from "../../controls/button.ts";
import Input from "../../controls/input.ts";

class CreateMenuPopoverComponent extends BaseComponent {
  constructor() {
    super("[data-testid='header-create-menu-popover']");
  }

  get createBoardBtn() {
    return new Button("[data-testid='header-create-board-button']");
  }

  get boardTitleInput() {
    return new Input("[data-testid='create-board-title-input']");
  }

  get submitBoardBtn() {
    return new Button("[data-testid='create-board-submit-button']");
  }
}

export default CreateMenuPopoverComponent;
