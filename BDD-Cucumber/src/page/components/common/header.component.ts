import BaseComponent from "./base.component.ts";
import Button from "../../controls/button.ts";

class HeaderComponent extends BaseComponent {
  constructor() {
    super("[data-testid='authenticated-header']");
  }

  get createMenuBtn() {
    return new Button("[data-testid='header-create-menu-button']");
  }

  get memberIcon() {
    return this.rootEl.$("[data-testid='header-member-menu-avatar']");
  }
}

export default HeaderComponent;
