type ElementSelector = string;

class BaseComponent {
  constructor(protected readonly rootSelector?: ElementSelector) {}

  get rootEl() {
    return $(this.rootSelector);
  }

  async getLastElement(list: WebdriverIO.ElementArray): Promise<WebdriverIO.Element> {
    const lastIndex = list.length - 1;
    await list[lastIndex].waitForDisplayed();

    return list[lastIndex];
  }

  async waitAndGetText(element: ChainablePromiseElement): Promise<string> {
    await element.waitForDisplayed();

    return element.getText();
  }

  async waitAndCheckForDisplayed(element: ChainablePromiseElement): Promise<boolean> {
    await element.waitForDisplayed();

    return element.isDisplayed();
  }
}

export default BaseComponent;
