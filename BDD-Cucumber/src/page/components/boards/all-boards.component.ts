import BaseComponent from "../common/base.component.ts";

class AllBoardsComponent extends BaseComponent {
  constructor() {
    super(".all-boards");
  }

  get userWorkspacesTitle() {
    return this.rootEl.$("h3.boards-page-section-header-name");
  }
}

export default AllBoardsComponent;
