import BaseComponent from "../common/base.component.ts";
import Button from "../../controls/button.ts";
import Input from "../../controls/input.ts";
import { WorkspaceInputName } from "../../../support/resources/types.ts";

class EditWorkspaceFormComponent extends BaseComponent {
  constructor() {
    super("form[aria-label='OrganizationDetailForm']");
  }

  get saveEditBtn() {
    return new Button("button.=save");
  }

  get cancelEditBtn() {
    return new Button("button.=cancel");
  }

  /**
   *
   * @param  name { "name" | "description" }
   * @returns {*}
   */

  get input() {
    const selectors = {
      name: "#displayName",
      description: "#desc",
    };

    return (name: WorkspaceInputName) => new Input(selectors[name]);
  }
}

export default EditWorkspaceFormComponent;
