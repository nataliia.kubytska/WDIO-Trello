import BaseComponent from "../common/base.component.ts";
import Button from "../../controls/button.ts";

class ListsComponent extends BaseComponent {
  constructor() {
    super("[data-testid='lists']");
  }

  get listComposerBtn() {
    return new Button("[data-testid='list-composer-button']");
  }

  get listNames() {
    return this.rootEl.$$("[data-testid='list-name']");
  }
}

export default ListsComponent;
