import BaseComponent from "../common/base.component.ts";
import Button from "../../controls/button.ts";
import Input from "../../controls/input.ts";

class ListComposerComponent extends BaseComponent {
  constructor() {
    super("form.vVqwaYKVgTygrk");
  }

  get listNameInput() {
    return new Input("[name='Enter list title…']");
  }

  get addListBtn() {
    return new Button("[data-testid='list-composer-add-list-button']");
  }

  get cancelListBtn() {
    return new Button("[data-testid='list-composer-cancel-button']");
  }
}

export default ListComposerComponent;
