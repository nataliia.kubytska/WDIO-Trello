import { BasePage } from "./base.page.ts";
import { HomeHeader } from "../components/index.ts";

export class HomePage extends BasePage {
  static readonly path: string = "/";
  readonly homeHeader: HomeHeader;

  constructor() {
    super(HomePage);
    this.homeHeader = new HomeHeader();
  }
}
