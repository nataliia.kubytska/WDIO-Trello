import { BasePage } from "./base.page.ts";
import { SignupForm } from "../components/index.ts";

export class SignupPage extends BasePage {
  static readonly path: string = "/signup";
  readonly signupForm: SignupForm;

  constructor() {
    super(SignupPage);
    this.signupForm = new SignupForm();
  }
}
