import { BasePage } from "./base.page.ts";
import { LoginForm } from "../components/index.ts";

export class LoginPage extends BasePage {
  static readonly path: string = "/login";
  readonly loginForm: LoginForm;

  constructor() {
    super(LoginPage);
    this.loginForm = new LoginForm();
  }
}
