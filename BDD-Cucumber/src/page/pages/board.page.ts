import { BasePage } from "./base.page.ts";
import { BoardHeader, Lists, ListComposer, List, CardComposer } from "../components/index.ts";

export class BoardPage extends BasePage {
  static readonly path: string = "/b/c26GhLBg/test-list-and-card";
  readonly boardHeader: BoardHeader;
  readonly lists: Lists;
  readonly listComposer: ListComposer;
  readonly list: List;
  readonly cardComposer: CardComposer;

  constructor() {
    super(BoardPage);
    this.boardHeader = new BoardHeader();
    this.lists = new Lists();
    this.listComposer = new ListComposer();
    this.list = new List();
    this.cardComposer = new CardComposer();
  }
}
