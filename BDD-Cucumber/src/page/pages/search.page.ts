import { BasePage } from "./base.page.ts";
import { SearchComponent } from "../components/index.ts";

export class SearchPage extends BasePage {
  static readonly path: string = "/search";
  readonly searchComponent: SearchComponent;

  constructor() {
    super(SearchPage);
    this.searchComponent = new SearchComponent();
  }
}
