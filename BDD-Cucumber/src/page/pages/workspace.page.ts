import { BasePage } from "./base.page.ts";
import { WorkspaceInfo, EditWorkspaceForm } from "../components/index.ts";

export class WorkspacePage extends BasePage {
  static readonly path: string = "/w/testworkspace04649910";
  readonly workspaceInfo: WorkspaceInfo;
  readonly editWorkspaceForm: EditWorkspaceForm;

  constructor() {
    super(WorkspacePage);
    this.workspaceInfo = new WorkspaceInfo();
    this.editWorkspaceForm = new EditWorkspaceForm();
  }
}
