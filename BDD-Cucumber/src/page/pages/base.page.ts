import { browser, $ } from "@wdio/globals";
import { Header, CreateMenuPopover } from "../components/index.ts";

interface Page {
  path: string;
}

export abstract class BasePage {
  readonly header: Header;
  readonly createMenuPopover: CreateMenuPopover;

  protected constructor(protected readonly page: Page) {
    this.header = new Header();
    this.createMenuPopover = new CreateMenuPopover();
  }

  get loggedoutHeader() {
    return $("[data-testid='logged-out-header-wide']");
  }

  async open(): Promise<void> {
    // await browser.maximizeWindow();
    await browser.url(`https://trello.com${this.page.path}`);
  }

  async openInNewWindow(): Promise<void> {
    await browser.newWindow(`https://trello.com${this.page.path}`);
  }
}
