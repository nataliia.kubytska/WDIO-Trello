import { HomePage } from "./home.page.ts";
import { LoginPage } from "./login.page.ts";
import { SignupPage } from "./signup.page.ts";
import { BoardsPage } from "./boards.page.ts";
import { BoardPage } from "./board.page.ts";
import { AccountPage } from "./account.page.ts";
import { SearchPage } from "./search.page.ts";
import { WorkspacePage } from "./workspace.page.ts";

type PageName = "home" | "signup" | "login" | "boards" | "board" | "account" | "search" | "workspace";

interface PageMap {
  home: HomePage;
  signup: SignupPage;
  login: LoginPage;
  boards: BoardsPage;
  board: BoardPage;
  account: AccountPage;
  search: SearchPage;
  workspace: WorkspacePage;
}

/** 
*
| "signup" | "login" | "boards" | "board" | "account" | "search" | "workspace" }
 * @returns { HomePage | SignupPage | LoginPage | BoardsPage | BoardPage | AccountPage | SearchPage | WorkspacePage }
 */

function page<T extends PageName>(name: T) {
  const items = {
    home: () => new HomePage(),
    signup: () => new SignupPage(),
    login: () => new LoginPage(),
    boards: () => new BoardsPage(),
    board: () => new BoardPage(),
    account: () => new AccountPage(),
    search: () => new SearchPage(),
    workspace: () => new WorkspacePage(),
  };

  const pageConstructor = items[name.toLowerCase() as keyof typeof items];

  if (!pageConstructor) {
    throw new Error(`No page found with name ${name}`);
  }

  return pageConstructor() as PageMap[T];
}

export { HomePage, SignupPage, LoginPage, BoardsPage, BoardPage, AccountPage, SearchPage, WorkspacePage, page };
