import { BasePage } from "./base.page.ts";
import { AllBoards } from "../components/index.ts";

export class BoardsPage extends BasePage {
  static readonly path: string = "/u/testuser25489/boards";
  readonly allBoards: AllBoards;

  constructor() {
    super(BoardsPage);
    this.allBoards = new AllBoards();
  }
}
