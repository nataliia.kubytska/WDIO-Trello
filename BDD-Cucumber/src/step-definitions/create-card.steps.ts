import { page } from "../page/index.ts";
import { reload } from "../support/resources/hooks.ts";
import { Given, When, Then, After } from "@wdio/cucumber-framework";
import { expect } from "chai";

const list = await page("board").list;
const cardComposer = await page("board").cardComposer;

Given(/^There is an existing list on the board$/, async (dataTable) => {
  const listName = dataTable.hashes()[0].listName;
  const listTitle = await list.waitAndGetText(list.listName);

  await expect(listTitle).to.equal(listName);
});

Given(/^I initiate the creation a the new card in the list$/, async () => {
  await list.cardComposerBtn.waitAndClick();
});

// Create a new card in the existing list

When(/^I provide a (.*) name for the new card creation$/, async (cardName) => {
  await cardComposer.cardNameInput.waitAndSetValue(cardName);
});

When(/I confirm the creation of a new card$/, async () => {
  await cardComposer.addCardBtn.waitAndClick();
});

Then(/^A new card with the provided (.*) name should be added to the list$/, async (cardName) => {
  const latestCard = await list.getLastElement(await list.cardsNames);
  const latestCardName = await latestCard.getText();

  await expect(latestCardName).to.equal(cardName, "latest card name title is not the expected one");
});

// Cancel the creation of a new card

When(/^I provide a (.*) name for the new card cancel$/, async (cardName) => {
  await cardComposer.cardNameInput.waitAndSetValue(cardName);
});

When(/^I cancel the creation of a new card$/, async () => {
  await cardComposer.cancelCardBtn.waitAndClick();
});

Then(/^A new card with the (.*) name should not be added to the list$/, async (cardName) => {
  const cardNamesArray = await list.cardsNames.map(async (cardName) => {
    return await cardName.getText();
  });

  await expect(cardNamesArray).to.not.include(cardName, "array contains cancelled card name");
});

After(
  {
    tags: "not @reset_edit_changes and not @reset_workspace_name and not @reset_workspace_description",
  },
  reload,
);
