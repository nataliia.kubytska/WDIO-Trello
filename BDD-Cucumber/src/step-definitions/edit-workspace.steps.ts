import { page } from "../page/index.ts";
import { resetWorkspaceName, resetWorkspaceDescription } from "../support/resources/hooks.ts";
import { Given, When, Then, After } from "@wdio/cucumber-framework";
import { expect } from "chai";

const workspaceInfo = await page("workspace").workspaceInfo;
const editWorkspaceForm = await page("workspace").editWorkspaceForm;

Given(/^I am on the Trello workspace page$/, async () => {
  await page("workspace").openInNewWindow();
  await page("workspace").header.memberIcon.waitForDisplayed();
});

Given(/^I see an existing workspace name$/, async (dataTable) => {
  const workspaceName = dataTable.hashes()[0].workspaceName;
  const currentWorkspaceName = await workspaceInfo.waitAndGetText(workspaceInfo.workspaceName);

  await expect(currentWorkspaceName).to.equal(workspaceName);
});

Given(/^I open a workspace edit form$/, async () => {
  await workspaceInfo.openEditWorkspaceFormBtn.waitAndClick();
});

// Edit workspace name

When(/^I edit workspace name with (.*) value$/, async (value) => {
  await editWorkspaceForm.input("name").waitAndAddValue(value);
});

When(/^I save my workspace name edits$/, async () => {
  await editWorkspaceForm.saveEditBtn.waitAndClick();
});

Then(/^A workspace name should get updated with (.*) value$/, async (value) => {
  const updatedWorkspaceName = await workspaceInfo.waitAndGetText(workspaceInfo.workspaceName);

  await expect(updatedWorkspaceName, "workspace name has not been updated with the expected value").to.include(value);
});

// Edit workspace description

When(/^I edit workspace description with (.*) value$/, async (value) => {
  await editWorkspaceForm.input("description").waitAndAddValue(value);
});

When(/^I save my workspace description edits$/, async () => {
  await editWorkspaceForm.saveEditBtn.waitAndClick();
});

Then(/^A workspace description should get updated with (.*) value$/, async (value) => {
  const updatedWorkspaceDescription = await workspaceInfo.waitAndGetText(workspaceInfo.workspaceDescription);

  await expect(updatedWorkspaceDescription, "workspace description has not been updated with the expected value").to.equal(value);
});

// Cancel workspace name editing

When(/^I edit workspace name with (.*) value for cancel$/, async (value) => {
  await editWorkspaceForm.input("name").waitAndAddValue(value);
});

When(/^I cancel the workspace name editing$/, async () => {
  await editWorkspaceForm.cancelEditBtn.waitAndClick();
});

Then(/^I should see no added (.*) value in my workspace (.*) name$/, async (value, workspaceName) => {
  const finalWorkspaceName = await workspaceInfo.waitAndGetText(workspaceInfo.workspaceName);

  await expect(finalWorkspaceName).to.not.include(value);
  await expect(finalWorkspaceName).to.equal(workspaceName, "workspace name updated even after cancelation");
});

After({ tags: "@reset_workspace_name" }, resetWorkspaceName);
After({ tags: "@reset_workspace_description" }, resetWorkspaceDescription);
