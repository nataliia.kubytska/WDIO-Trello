import { page } from "../page/index.ts";
import { createRegExp } from "../support/resources/constants.ts";
import { Given, When, Then } from "@wdio/cucumber-framework";
import { expect } from "chai";

const signupForm = page("signup").signupForm;

Given(/^I am on the Trello sign-up page$/, async () => {
  await page("home").open();
  await page("home").homeHeader.signupPageBtn.waitAndClick();
});

When(/^I perform sign-up with invalid (.*) address$/, async (email) => {
  await signupForm.performSignup(email);
});

Then(/^I should see an appropriate error message$/, async () => {
  const messageDisplayed = await signupForm.waitAndCheckForDisplayed(signupForm.errorMessage);
  const errorMessage = await signupForm.errorMessage.getText();

  await expect(messageDisplayed, "error message did not display").to.be.true;
  await expect(errorMessage).to.match(createRegExp("ERROR_SIGNUP_MESSAGE"));
});
