import { page } from "../page/index.ts";
import { resetProfileUsername } from "../support/resources/hooks.ts";
import { Given, When, Then, After } from "@wdio/cucumber-framework";
import { expect } from "chai";

const editForm = await page("account").editUserForm;
const userInfo = await page("account").userInfo;

Given(/^I am on the Trello edit profile page$/, async () => {
  await page("account").openInNewWindow();
});

Given(/^I navigate to the edit user profile form$/, async () => {
  await page("account").memberNavbar.profileTab.waitAndClick();

  const formDisplayed = await editForm.waitAndCheckForDisplayed(editForm.rootEl);
  await expect(formDisplayed, "form did not display").to.be.true;
});

// Edit profile username using valid characters

When(/^I edit profile username with (.*) value using valid characters$/, async (value) => {
  await editForm.input("username").waitAndAddValue(value);
});

When(/^I save my valid edits to my username$/, async () => {
  await editForm.saveEditBtn.waitAndClick();
});

Then(/^A success message (.*) confirming the update of my username should appear$/, async (message) => {
  const popupText = await editForm.waitAndGetText(editForm.successPopup);
  const popupDisplayed = await editForm.successPopup.isDisplayed();

  await expect(popupDisplayed, "success username editing popup did not display").to.be.true;
  await expect(popupText).to.equal(message);
});

Then(/^My profile username should get updated with (.*) value$/, async (value) => {
  await browser.waitUntil(async () => {
    const url = await browser.getUrl();
    return url.includes(value);
  });

  const username = await userInfo.username.getText();
  await expect(username).to.include(value, "username has not been updated with the expected value");
});

// Cancel profile username editing

Given(/^Current profile have (.*) username$/, async (username) => {
  const initialUsername = await userInfo.waitAndGetText(userInfo.username);

  await expect(initialUsername).to.equal(username);
});

When(/^I edit username in the edit profile form using (.*) value$/, async (value) => {
  await editForm.input("username").waitAndAddValue(value);
});

When(/^I cancel the profile username editing$/, async () => {
  await browser.refresh();
});

Then(/^I should see no added (.*) value in my profile (.*) username$/, async (value, username) => {
  const finalUsername = await userInfo.waitAndGetText(userInfo.username);

  await expect(finalUsername).to.not.include(value);
  await expect(finalUsername).to.equal(username, "username updated even after cancelation");
});

// Edit profile username using invalid credentials

When(/^I edit profile username using invalid (.*) characters$/, async (characters) => {
  await editForm.input("username").waitAndAddValue(characters);
});

When(/^I try to save my invalid edits to my username$/, async () => {
  await editForm.saveEditBtn.waitAndClick();
});

Then(/^An error message (.*) should appear$/, async (message) => {
  const errorText = await editForm.waitAndGetText(editForm.errorMessage);
  const errorMessageDisplayed = await editForm.errorMessage.isDisplayed();

  await expect(errorMessageDisplayed, "error message did not display").to.be.true;
  await expect(errorText).to.include(message, "error message didn't include expected text");
});

Then(/^My profile (.*) username should remain unchanged$/, async (username) => {
  const currentUsername = await userInfo.waitAndGetText(userInfo.username);

  await expect(currentUsername).to.equal(username, "username updated with invalid characters");
});

After({ tags: "@reset_edit_changes" }, resetProfileUsername);
