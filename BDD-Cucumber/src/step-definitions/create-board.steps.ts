import { page, BoardsPage } from "../page/index.ts";
import { Given, When, Then } from "@wdio/cucumber-framework";
import { expect } from "chai";

//Create a new board

Given(/^I am on the dashboard page$/, async () => {
  await page("boards").header.memberIcon.waitForDisplayed();

  expect(await browser.getUrl()).to.include(BoardsPage.path);
});

When(/^I initiate the creation of a new board$/, async () => {
  await page("boards").header.createMenuBtn.waitAndClick();
  await page("boards").createMenuPopover.createBoardBtn.waitAndClick();
});

When(/^Provide a (.*) name for the board$/, async (boardName) => {
  await page("boards").createMenuPopover.boardTitleInput.waitAndSetValue(boardName);
});

When(/^Confirm the creation of a new board$/, async () => {
  await page("boards").createMenuPopover.submitBoardBtn.waitAndClick();
});

Then(/^A new board with the specified (.*) name should be created$/, async (boardName) => {
  const boardTitle = await page("board").boardHeader.boardTitle.getText();

  await expect(boardTitle).to.equal(boardName);
});
