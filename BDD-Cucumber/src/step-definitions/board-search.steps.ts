import { page } from "../page/index.ts";
import { reload } from "../support/resources/hooks.ts";
import { Given, When, Then, After } from "@wdio/cucumber-framework";
import { expect } from "chai";

const search = await page("search").searchComponent;

Given(/^I navigate to the search page$/, async () => {
  await page("search").openInNewWindow();
  await page("search").header.memberIcon.waitForDisplayed();
});

//Search for a specific existing board (1) and Search for boards with similar names (2)

When(/^I initiate a search with the (.*) value$/, async (query) => {
  await search.searchInput.waitAndSetValue(query);
  await search.boards.waitForDisplayed();
});

Then(/^The search result should only include boards with (.*) in the name$/, async (query) => {
  const boardNames = await search.searchResultItems.map(async (resultItem: WebdriverIO.Element) => {
    return await resultItem.getAttribute("title");
  });
  const regex = new RegExp(query, "i");

  await boardNames.forEach((name: string) => expect(name).to.match(regex));
});

Then(/^I can access the board from the search results$/, async () => {
  await search.searchResultItems[0].click();
});

Then(/^It has a (.*) title that contains the value of the search query$/, async (title) => {
  const boardTitle = await page("board").boardHeader.boardTitle.getText();

  await expect(boardTitle).to.equal(title);
});

//Search for a non-existent board

When(/^I initiate a search for non-existent board with the (.*) value$/, async (query) => {
  await search.searchInput.waitAndSetValue(query);
});

Then(/^An appropriate message should display: (.*)$/, async (message) => {
  const searchMessage = await search.waitAndGetText(search.noFoundBoardsMessage);

  await expect(searchMessage).to.include(message);
});

After(
  {
    tags: "not @reset_edit_changes and not @reset_workspace_name and not @reset_workspace_description",
  },
  reload,
);
