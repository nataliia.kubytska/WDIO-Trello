import { page, BoardsPage } from "../page/index.ts";
import { reload } from "../support/resources/hooks.ts";
import { createRegExp } from "../support/resources/constants.ts";
import { Given, When, Then, After } from "@wdio/cucumber-framework";
import { expect } from "chai";

const loginForm = page("login").loginForm;
const allBoards = page("boards").allBoards;

Given(/^I am on the Trello login page$/, async () => {
  await page("login").open();
});

//login with the valid credentials
When(/^I perform login with the valid credentials$/, async () => {
  await loginForm.performLogin(process.env.EMAIL, process.env.PASSWORD);
});

Then(/^I am redirected to my Trello dashboard page$/, async () => {
  await page("boards").header.memberIcon.waitForDisplayed();
  expect(await browser.getUrl()).to.include(BoardsPage.path);
});

Then(/^I see my workspace$/, async () => {
  const userWorkspacesTitle = await allBoards.waitAndGetText(allBoards.userWorkspacesTitle);

  await expect(userWorkspacesTitle).to.match(createRegExp("USER_WORKSPACE_TITLE"));
});

// login with the incorrect credentials
When(/^I perform login with incorrect (.*) email and (.*) password$/, async (email, password) => {
  await loginForm.performLogin(email, password);
});

Then(/^I see an error message$/, async () => {
  const errorMessage = await loginForm.waitAndGetText(loginForm.errorLoginMessage);
  await expect(errorMessage).to.match(createRegExp("ERROR_LOGIN_MESSAGE"));
});

// password reset request
When(/^I request a recovery link to be sent to my email address$/, async () => {
  await loginForm.passwordResetRequest(process.env.EMAIL);
});

Then(/^I see a message that an email with a recovery link is sent to my inbox$/, async () => {
  const emailSentMessage = await loginForm.waitAndGetText(loginForm.emailSentMessage);
  const messageDisplayed = await loginForm.emailSentMessage.isDisplayed();

  await expect(messageDisplayed, "message did not display").to.be.true;
  await expect(emailSentMessage).to.match(createRegExp("RESET_PASSWORD_SUCCESS_MESSAGE"));
});

After(
  {
    tags: "not @reset_edit_changes and not @reset_workspace_name and not @reset_workspace_description",
  },
  reload,
);
