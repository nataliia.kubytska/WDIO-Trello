import { page } from "../page/index.ts";
import { reload } from "../support/resources/hooks.ts";
import { Given, When, Then, After } from "@wdio/cucumber-framework";
import { expect } from "chai";

const lists = await page("board").lists;
const listComposer = await page("board").listComposer;

Given(/^I initiate the creation of a new list in the board$/, async () => {
  await lists.listComposerBtn.waitAndClick();
});

// Create a new list in the board

When(/^I provide a (.*) name for the new list creation$/, async (listName) => {
  await listComposer.listNameInput.waitAndSetValue(listName);
});

When(/^I confirm the creation of a new list$/, async () => {
  await listComposer.addListBtn.waitAndClick();
});

Then(/^A new list with the provided (.*) name should be added to the board$/, async (listName) => {
  const latestList = await lists.getLastElement(await lists.listNames);
  const latestListName = await latestList.getText();

  await expect(latestListName).to.equal(listName, "latest list name title is not the expected one");
});

// Cancel the creation of a new list

When(/^I provide a (.*) name for the new list cancel$/, async (listName) => {
  await listComposer.listNameInput.waitAndSetValue(listName);
});

When(/^I cancel the creation of a new list$/, async () => {
  await listComposer.cancelListBtn.waitAndClick();
});

Then(/^A new list with the (.*) name should not be added to the list$/, async (listName) => {
  const listNamesArray = await lists.listNames.map(async (listName) => {
    return await listName.getText();
  });

  await expect(listNamesArray).to.not.include(listName, "array contains this list name");
});

After(
  {
    tags: "not @reset_edit_changes and not @reset_workspace_name and not @reset_workspace_description",
  },
  reload,
);
