import { page } from "../page/index.ts";
import { Given } from "@wdio/cucumber-framework";

Given(/^I am logged into my Trello account$/, async () => {
  await page("login").open();
  await page("login").loginForm.performLogin(process.env.EMAIL, process.env.PASSWORD);
});

Given(/^I am on the board page$/, async () => {
  await page("board").openInNewWindow();
  await page("board").header.memberIcon.waitForDisplayed();
});
