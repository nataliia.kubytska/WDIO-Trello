@parallel
@smoke
Feature: Trello Card Creation
  As a Trello user,
  I want to be able to create a new card in the existed list of the existed board

  Background:
    Given I am logged into my Trello account
    And I am on the board page
    And There is an existing list on the board
      | listName  |
      | Test list |
    And I initiate the creation a the new card in the list

  Scenario: Create a new card in the existing list
    When I provide a <cardName> name for the new card creation
    And I confirm the creation of a new card
    Then A new card with the provided <cardName> name should be added to the list

    Examples:
      | cardName  |
      | Card Name |

  Scenario: Cancel the creation of a new card
    When I provide a <cardName> name for the new card cancel
    And I cancel the creation of a new card
    Then A new card with the <cardName> name should not be added to the list

    Examples:
      | cardName    |
      | Card Cancel |
