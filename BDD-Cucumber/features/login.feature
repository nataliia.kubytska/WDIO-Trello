@parallel
Feature: Trello Login
  As a Trello user
  I want to able to log in successfully only with valid credentials.

  Background:
    Given I am on the Trello login page

  @smoke
  Scenario: Login with the valid credentials
    When I perform login with the valid credentials
    Then I am redirected to my Trello dashboard page
    And I see my workspace

  @negative_scenario
  Scenario: Login with the incorrect credentials
    When I perform login with incorrect <email> email and <password> password
    Then I see an error message

    Examples:
      | email                       | password     |
      | test.user010101111@gmail.co | test.passwor |

  Scenario: Password reset request while login
    When I request a recovery link to be sent to my email address
    Then I see a message that an email with a recovery link is sent to my inbox
