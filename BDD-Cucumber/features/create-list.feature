@parallel
@smoke
Feature: Trello List Creation
  As a Trello user,
  I want to be able to create a new list in the existed board

  Background:
    Given I am logged into my Trello account
    And I am on the board page
    And I initiate the creation of a new list in the board

  Scenario: Create a new list in the board
    When I provide a <listName> name for the new list creation
    And I confirm the creation of a new list
    Then A new list with the provided <listName> name should be added to the board

    Examples:
      | listName  |
      | List Name |

  Scenario: Cancel the creation of a new list
    When I provide a <listName> name for the new list cancel
    And I cancel the creation of a new list
    Then A new list with the <listName> name should not be added to the list

    Examples:
      | listName    |
      | List Cancel |
