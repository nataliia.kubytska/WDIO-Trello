@sequential
@regression
Feature: Trello Workspace Editing
  As a Trello user,
  I want to be able to edit my workspace information

  Background:
    Given I am logged into my Trello account
    And I am on the Trello workspace page
    And I see an existing workspace name
      | workspaceName  |
      | Test Workspace |
    And I open a workspace edit form

  @reset_workspace_name
  Scenario: Edit workspace name
    When I edit workspace name with <value> value
    And I save my workspace name edits
    Then A workspace name should get updated with <value> value

    Examples:
      | value   |
      | testing |

  @reset_workspace_description
  Scenario: Edit workspace description
    When I edit workspace description with <value> value
    And I save my workspace description edits
    Then A workspace description should get updated with <value> value

    Examples:
      | value   |
      | testing |

  Scenario: Cancel workspace name editing
    When I edit workspace name with <value> value for cancel
    And I cancel the workspace name editing
    Then I should see no added <value> value in my workspace <workspaceName> name

    Examples:
      | workspaceName  | value   |
      | Test Workspace | testing |
