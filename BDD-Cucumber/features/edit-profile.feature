@sequential
@regression
Feature: Trello User Profile Editing
  As a Trello user,
  I want to be able to edit user profile information

  Background:
    Given I am logged into my Trello account
    And I am on the Trello edit profile page
    And I navigate to the edit user profile form

  @reset_edit_changes
  Scenario: Edit profile username using valid characters
    When I edit profile username with <value> value using valid characters
    And I save my valid edits to my username
    Then A success message <message> confirming the update of my username should appear
    And My profile username should get updated with <value> value

    Examples:
      | value   | message |
      | testing | Saved   |

  Scenario: Cancel profile username editing
    And Current profile have <username> username
    When I edit username in the edit profile form using <value> value
    And I cancel the profile username editing
    Then I should see no added <value> value in my profile <username> username

    Examples:
      | username       | value   |
      | @testuser25489 | testing |

  @negative_scenario
  Scenario: Edit profile username using invalid credentials
    When I edit profile username using invalid <characters> characters
    And I try to save my invalid edits to my username
    Then An error message <message> should appear
    And My profile <username> username should remain unchanged

    Examples:
      | characters | message             | username       |
      | *^         | Username is invalid | @testuser25489 |
