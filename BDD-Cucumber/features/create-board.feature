@parallel
@regression
Feature: Trello Board Creating
  As a Trello user,
  I want to be able to create a new board in my workspace

  Scenario: Create a new board
    Given I am logged into my Trello account
    And I am on the dashboard page
    When I initiate the creation of a new board
    And Provide a <boardName> name for the board
    And Confirm the creation of a new board
    Then A new board with the specified <boardName> name should be created

    Examples:
      | boardName  |
      | Board Name |
