@parallel
Feature: Trello Board Search
  As a Trello user
  I want to be able to find specified board or multiple boards with similar names

  Background:
    Given I am logged into my Trello account
    And I navigate to the search page

  @smoke
  Scenario Outline: Search for the existing boards
    When I initiate a search with the <query> value
    Then The search result should only include boards with <query> in the name
    And I can access the board from the search results
    And It has a <boardTitle> title that contains the value of the search query

    Examples:
      | query             | boardTitle          |
      | Board Test Search | Board Test Search   |
      | for test          | Task Board for Test |

  @negative_scenario
  Scenario: Search for a non-existent board
    When I initiate a search for non-existent board with the <query> value
    Then An appropriate message should display: <message>

    Examples:
      | query              | message                                        |
      | Non-existent Board | We couldn't find anything matching that search |
