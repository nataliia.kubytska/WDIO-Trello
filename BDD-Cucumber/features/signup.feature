@parallel
Feature: Trello Sign-up
  As a Trello user
  I want to able to sign up successfully only with valid credentials.

  @negative_scenario
  Scenario: Sign-up with the invalid credential
    Given I am on the Trello sign-up page
    When I perform sign-up with invalid <email> address
    Then I should see an appropriate error message

    Examples:
      | email                       |
      | test.user010101111@gmailcom |
